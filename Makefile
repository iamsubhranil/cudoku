CXXFLAGS = -Wall -Wextra

RM=rm -f
LDFLAGS=-lncursesw

SRCS=main.cpp sudoku.cpp cui.cpp optionparser.cpp cdsf.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

ifeq "$(COUNT)" ""
	COUNT=1000
endif

all: release

release: CXXFLAGS += -O3
release: LDFLAGS += -s
release: cudoku

debug: CXXFLAGS += -g3 -DDEBUG
debug: clean cudoku

bench: CXXFLAGS += -O3 -DBENCH -DCOUNT=$(COUNT)
bench: clean cudoku

profile: CXXFLAGS += -g3 -pg -DBENCH -DCOUNT=$(COUNT)
profile: clean cudoku

solve: CXXFLAGS += -DSOLVE
solve: release

cudoku: $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o cudoku

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend

include .depend

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $<
