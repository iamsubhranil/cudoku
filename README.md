An ncurses based Sudoku game written in C++.
============================================

Building
--------

Requirements : **ncursesw** (with wide character support)

1. Manual compilation
```
<your_favourite_c++_compiler> *.cpp -lncursesw -O3 -o cudoku
```

2. Using Makefile
```
make all -j
```

Running
-------
```
./cudoku
```

Movement
--------

1. Use <kbd>W</kbd><kbd>A</kbd><kbd>S</kbd><kbd>D</kbd> or <kbd>↑</kbd><kbd>↓</kbd><kbd>←</kbd><kbd>→</kbd> to roam around cells.

2. Press <kbd>1-9</kbd> to set the cell with the specified value.

3. Use <kbd>0</kbd>/<kbd>X</kbd>/<kbd>Delete</kbd>/<kbd>Backspace</kbd> to clear a cell.

4. Press <kbd>C</kbd> to check your solution. If it is wrong, the first cell to violate the property will be marked as red.

5. Press <kbd>Q</kbd> to quit.

6. Press <kbd>N</kbd> to generate a new sudoku board.

7. Press <kbd>R</kbd> to reload the configuration (See [cudoku.conf](https://gitlab.com/iamsubhranil/cudoku/blob/develop/cudoku.conf) for details).
