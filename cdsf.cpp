#include "cdsf.h"
#include "sudoku.h"
#include <fstream>
#include <iostream>

using namespace std;

// Implementation of Serializer class

void Serializer::checkMagic() {
	uint32_t rMagic = r32();
	if(this->Magic() != rMagic) {
		throw MAGIC_NOT_MATCHED;
	}
}

void Serializer::deserializeFromBytes(char **data) {
	this->RWPointer = data;
	checkMagic();
	loadFromBytes();
}

void Serializer::deserializeFromBytes(Serializer *s) {
	deserializeFromBytes(s->RWPointer);
}

void Serializer::serializeIntoBytes(char **bytes, bool extend) {
	char *orig;
	if(extend) {
		orig = *bytes = (char *)realloc(*bytes, this->size());
		memset(*bytes, 0, this->size());
	}
	this->RWPointer = bytes;
	w32(this->Magic());
	storeIntoBytes();
	if(extend)
		*bytes = orig;
}

void Serializer::serializeIntoBytes(char **bytes, size_t *written) {
	*bytes          = (char *)realloc(*bytes, *written + this->size());
	char *n         = &(*bytes[*written]);
	this->RWPointer = &n;
	storeIntoBytes();
	*written += this->size();
}

void Serializer::serializeIntoBytes(Serializer *s) {
	serializeIntoBytes(s->RWPointer, false);
}

Serializer::~Serializer() {}

// Implementation of ElapsedTime class

ElapsedTime::ElapsedTime() {
	Hours = Minutes = Seconds = 0;
}

uint32_t ElapsedTime::Magic() {
	return ElapsedTimeMagic;
}

size_t ElapsedTime::size() {
	return sizeof(ElapsedTimeMagic) + sizeof(Hours) + sizeof(Minutes) +
	       sizeof(Seconds);
}

void ElapsedTime::loadFromBytes() {
	Hours = r8();

	Minutes = r8();

	Seconds = r8();
}

void ElapsedTime::storeIntoBytes() {
	w8(Hours);
	w8(Minutes);
	w8(Seconds);
}

void ElapsedTime::tick() {
	Seconds++;
	Minutes += Seconds > 59;
	Hours += Minutes > 59;
	if(Minutes > 59)
		Minutes %= 60;
	if(Seconds > 59)
		Seconds %= 60;
}

void ElapsedTime::reset() {
	Seconds = Minutes = Hours = 0;
}

int ElapsedTime::getHours() {
	return Hours;
}

int ElapsedTime::getMinutes() {
	return Minutes;
}

int ElapsedTime::getSeconds() {
	return Seconds;
}

ElapsedTime ElapsedTime::operator=(const ElapsedTime &et) {
	Seconds = et.Seconds;
	Minutes = et.Minutes;
	Hours   = et.Hours;
	return *this;
}

ostream &operator<<(ostream &os, const ElapsedTime &et) {
	return os << (int)et.Hours << ":" << (int)et.Minutes << ":"
	          << (int)et.Seconds;
}

// Implementation of GameState class

#define SET(arr, bit) (arr[(bit) / 8] |= 1UL << ((bit)&7))
#define RESET(arr, bit) (arr[(bit) / 8] &= ~(1UL << ((bit)&7)))
#define TOGGLE(arr, bit) (arr[(bit) / 8] ^= 1UL << ((bit)&7))
#define ISSET(arr, bit) ((arr[(bit) / 8] >> ((bit)&7)) & 1U)

#define MAX_DIFFICULTY 3

GameState::GameState(Sudoku *s, const ElapsedTime &e, int diff, uint8_t x,
                     uint8_t y)
    : Elapsed(e) {
	GameState();
	update(s, e, diff, x, y);
}

GameState::GameState() {
	Difficulty = 0;
	for(int i = 0; i < 41; i++) numbers[i] = 0;
	for(int j = 0; j < 11; j++) pred[j] = 0;
}

size_t GameState::size() {
	return sizeof(GameStateMagic) + sizeof(Difficulty) + Elapsed.size() +
	       sizeof(numbers[0]) * 41 + sizeof(pred[0]) * 11 + sizeof(PosX) +
	       sizeof(PosY);
}

uint32_t GameState::Magic() {
	return GameStateMagic;
}

void GameState::loadFromBytes() {
	uint8_t rDifficulty = r8();

	if(rDifficulty > MAX_DIFFICULTY) {
		throw INVALID_DIFFICULTY;
	}

	Difficulty = rDifficulty;

	Elapsed.deserializeFromBytes(this);

	for(int i = 0; i < 41; i++) numbers[i] = r8();
	for(int i = 0; i < 11; i++) pred[i] = r8();

	PosX = r8();
	PosY = r8();
}

void GameState::storeIntoBytes() {
	w8(Difficulty);
	Elapsed.serializeIntoBytes(this);

	for(int i = 0; i < 41; i++) w8(numbers[i]);
	for(int i = 0; i < 11; i++) w8(pred[i]);

	w8(PosX);
	w8(PosY);
}

void GameState::update(Sudoku *s, const ElapsedTime &e, int diff, uint8_t x,
                       uint8_t y) {
	for(int i = 0; i < 41; i++) numbers[i] = 0;
	for(int j = 0; j < 11; j++) pred[j] = 0;
	for(int i = 0; i < 81; i++) {
		int f = s->at(i / 9, i % 9);
		numbers[i / 2] |= (i & 1) ? f : (f << 4);
		if(s->is_predefined(i / 9, i % 9))
			SET(pred, i);
		else
			RESET(pred, i);
	}
	Difficulty = diff;
	Elapsed    = e;
	PosX       = x;
	PosY       = y;
}

void GameState::populateSudoku(Sudoku *s) {
	s->reset();
	for(int i = 0; i < 81; i++) {
		int val;
		if(i & 1) {
			val = numbers[i / 2] & 0x0f;
		} else {
			val = (numbers[i / 2] & 0xf0) >> 4;
		}
		bool pr = ISSET(pred, i);
		s->set(i / 9, i % 9, val, pr);
	}
}

ElapsedTime GameState::getElapsedTime() {
	return Elapsed;
}

void GameState::getPosition(uint8_t *x, uint8_t *y) {
	*x = PosX;
	*y = PosY;
}

ostream &operator<<(ostream &os, const GameState &gs) {
	os << "\tMagic : " << gs.GameStateMagic << endl;
	os << "\tDifficulty : " << gs.Difficulty << endl;
	os << "\tElapsed Time : " << gs.Elapsed << endl;
	return os;
}

// Implementation of CudokuSaveFile class

CudokuSaveFile::CudokuSaveFile() {
	Size = Checksum = SaveCount = 0;
	States                      = vector<GameState>();
}

uint32_t CudokuSaveFile::Magic() {
	return CudokuSaveFileMagic;
}

size_t CudokuSaveFile::size() {
	return sizeof(CudokuSaveFileMagic) + sizeof(Delimiter) + sizeof(Size) +
	       sizeof(Checksum) + sizeof(VMajor) + sizeof(VMinor) +
	       sizeof(SaveCount) + SaveCount * GameState().size() +
	       9 * ElapsedTime().size();
}

void CudokuSaveFile::loadFromBytes() {
	uint32_t rDelimiter = r32();

	if(rDelimiter != Delimiter) {
		throw DELIMITER_MISMATCH;
	}

	Size = r64();

	Checksum = r64();

	uint8_t rMajor = r8();
	if(rMajor != VMajor) {
		throw MAJOR_VERSION_MISMATCH;
	}

	uint8_t rMinor = r8();

	if(rMinor != VMinor) {
		throw MINOR_VERSION_MISMATCH;
	}

	SaveCount = r8();

	States.resize(SaveCount);

	for(uint8_t i = 0; i < SaveCount; i++) {
		States[i].deserializeFromBytes(this);
	}

	for(int i = 0; i < 9; i++) {
		HiScores[i].deserializeFromBytes(this);
	}
}

void CudokuSaveFile::storeIntoBytes() {

	w32(Delimiter);

	w64(size());

	w64(Checksum);

	w8(VMajor);

	w8(VMinor);

	w8(SaveCount);

	for(uint8_t i = 0; i < SaveCount; i++) {
		States[i].serializeIntoBytes(this);
	}

	for(int i = 0; i < 9; i++) {
		HiScores[i].serializeIntoBytes(this);
	}
}

int CudokuSaveFile::saveGame(Sudoku *s, const ElapsedTime &et, int difficulty,
                             uint8_t x, uint8_t y, int GameNo) {
	if(GameNo == -1) {
		States.push_back(GameState(s, et, difficulty, x, y));
		SaveCount++;
	} else {
		States[GameNo].update(s, et, difficulty, x, y);
	}
	return SaveCount - 1;
}

int CudokuSaveFile::loadLastGame(Sudoku *s, ElapsedTime &e, int *x, int *y) {
	if(SaveCount > 0) {
		States.back().populateSudoku(s);
		e = States.back().getElapsedTime();
		uint8_t px, py;
		States.back().getPosition(&px, &py);
		*x = px;
		*y = py;
	} else {
		s->generate();
		s->remove_at_random(50);
		e = ElapsedTime();
		*x = -1;
		*y = -1;
		States.push_back(GameState(s, e, 0, *x, *y));
		SaveCount++;
	}
	return SaveCount - 1;
}

ostream &operator<<(ostream &os, const CudokuSaveFile &csf) {
	os << "CudokuSaveFile" << endl;
	os << "==============" << endl;
	os << "Magic : " << csf.CudokuSaveFileMagic << endl;
	os << "Version : " << (int)csf.VMajor << "." << (int)csf.VMinor << endl;
	os << "Saved Games : " << (int)csf.SaveCount << endl;
	for(int i = 0; i < csf.SaveCount; i++) {
		os << "Game #" << i << endl;
		os << "==========" << endl;
		os << csf.States[i] << endl;
	}
	for(int i = 0; i < 9; i++) {
		os << "Hiscore #" << i << " : " << csf.HiScores[i] << endl;
	}
	return os;
}

// Actual I/O helpers

void CudokuSaveFile::loadFromFile(const char *loc) {
	ifstream input(loc, ios::in | ios::binary | ios::ate);
	if(!input.is_open()) {
		throw FILE_NOT_FOUND;
	}
	// Find the real size of the file
	uint64_t realSize = input.tellg();
	// The size of the file must be
	// greater than an empty save file
	if(realSize < size()) {
		cout << "[Error] Size of the file (" << realSize
		     << " bytes) is less than expected (" << size() << " bytes)!"
		     << endl;
		throw FILESIZE_LESS_THAN_MINIMUM;
	}

	input.seekg(0);
	char           rMagic[8];
	input.read(&rMagic[0], 4);
	if(*(uint32_t *)(&rMagic[0]) == Magic()) {
		// Skip the delimiter
		input.read(&rMagic[0], 4);
		// Read the size
		input.read(&rMagic[0], 8);
		uint64_t expectedSize = *(uint64_t *)(&rMagic[0]);

		if(expectedSize != realSize) {
			cout << "[Error] Size of the file (" << realSize
			     << " bytes) is different from the expected "
			        "size ("
			     << expectedSize << " bytes)!" << endl;
			throw SIZE_MISMATCH;
		}

		input.seekg(0);
		char *data = new char[realSize];
		input.read(data, realSize);
		input.close();
		char *bak = data;
		deserializeFromBytes(&bak);
		delete[] data;
	} else
		throw MAGIC_NOT_MATCHED;
}

void CudokuSaveFile::storeIntoFile(const char *loc) {
	char *data = NULL;
	serializeIntoBytes(&data, true);
	ofstream output(loc, ios::out | ios::binary);
	output.write(data, size());
	output.close();
	free(data);
}
/*
int main(int argc, char *argv[]) {
    if(argc < 2) {
        cout << "Provide the filename!" << endl;
        return 1;
    }

    try {
        CudokuSaveFile csf;
        csf.storeIntoFile(argv[1]);
        csf.loadFromFile(argv[1]);
}
*/
