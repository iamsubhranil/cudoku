#pragma once

#include "sudoku.h"
#include <cstdint>
#include <malloc.h>
#include <ostream>
#include <string.h>

// Exception types

typedef enum {
	MAGIC_NOT_MATCHED          = 0,
	INVALID_DIFFICULTY         = 1,
	MAJOR_VERSION_MISMATCH     = 2,
	MINOR_VERSION_MISMATCH     = 3,
	FILE_NOT_FOUND             = 4,
	FILESIZE_LESS_THAN_MINIMUM = 5,
	SIZE_MISMATCH              = 6,
	DELIMITER_MISMATCH         = 7,
} ReadError;

#define VMAJOR 0
#define VMINOR 2

class Serializer {
  private:
	void   checkMagic();
	// Points to the address of next
	// read/write byte
	char **RWPointer = NULL;

  protected:
	// Returns the magic bytes of the object
	// to be serialized
	virtual uint32_t Magic() = 0;
	// Given a series of bytes, this method will
	// perform deserialization of the class.
	// Deserialization should typically be
	// performed by the use of various read
	// calls below, reading one member at a time.
	virtual void loadFromBytes() = 0;
	// This method will perform the serialization
	// of the class in an array of bytes.
	// Serialization should typically be performed
	// by the use of various write meethods below,
	// writing one member at a time.
	virtual void storeIntoBytes() = 0;
	// This method will return the total size of all
	// members when serialized.
	virtual size_t size() = 0;
	// Virtual destructor
	virtual ~Serializer();

	// Read write helpers
	// Just call them for any read/write.
	// Everything else is managed by Serializer
	// internally.
	inline uint64_t r64() {
		uint64_t d = (*(uint64_t *)*RWPointer);
		*RWPointer += sizeof(uint64_t);
		return d;
	}
	inline uint32_t r32() {
		uint32_t d = (*(uint32_t *)*RWPointer);
		*RWPointer += sizeof(uint32_t);
		return d;
	}
	inline uint16_t r16() {
		uint16_t d = (*(uint16_t *)*RWPointer);
		*RWPointer += sizeof(uint16_t);
		return d;
	}
	inline uint8_t r8() {
		uint8_t d = (*(uint8_t *)*RWPointer);
		*RWPointer += sizeof(uint8_t);
		return d;
	}
	inline void *rv(size_t size) {
		void *v = (void *)(*RWPointer);
		*RWPointer += size;
		return v;
	}

	inline void w64(uint64_t val) {
		memcpy(*RWPointer, (void *)&val, sizeof(val));
		*RWPointer += sizeof(uint64_t);
	}
	inline void w32(uint32_t val) {
		memcpy(*RWPointer, (void *)&val, sizeof(val));
		*RWPointer += sizeof(uint32_t);
	}
	inline void w16(uint16_t val) {
		memcpy(*RWPointer, (void *)&val, sizeof(val));
		*RWPointer += sizeof(uint16_t);
	}
	inline void w8(uint8_t val) {
		memcpy(*RWPointer, (void *)&val, sizeof(val));
		*RWPointer += sizeof(uint8_t);
	}
	inline void wv(void *v, size_t size) {
		memcpy(*RWPointer, v, size);
		*RWPointer += size;
	}

  public:
	// Performs checkMagic first, and then calls
	// loadFromBytes, which then perform load
	// for individual classes. After deserialization
	// is done, *data is set to the end of last read
	// byte.
	void deserializeFromBytes(char **data);
	// First extends the given bytes if mentioned,
	// then calls storeIntoBytes to perform store for
	// individual classes.
	// extend marks whether to realloc '*data' to hold
	// the objects.
	// If extend is false, the address of the last byte
	// written will be assigned to *data. Otherwise, the realloced
	// address of *data is kept as it is.
	void serializeIntoBytes(char **data, bool extend = false);
	// Same as previous method with the following changes :
	// reallocs 'data' to written + size(), and then
	// starts serializing from data[*written]
	// Saves the realloced address in *RWPointer
	void serializeIntoBytes(char **data, size_t *written);

	// For nested serialization, use the following methods.
	// Call the method on the child object, passing 'this'
	// pointer of the parent as argument.
	void deserializeFromBytes(Serializer *s);
	void serializeIntoBytes(Serializer *s);
};

class ElapsedTime : public Serializer {
  private:
	const uint32_t ElapsedTimeMagic = 0x454c5400; // ELT\0 (Elapsed Time\0)
	uint8_t        Hours;
	uint8_t        Minutes;
	uint8_t        Seconds;
	void           loadFromBytes();
	void           storeIntoBytes();
	uint32_t       Magic();

  public:
	size_t size();
	void   tick(); // progress one second
	void   reset();
	int    getHours();
	int    getMinutes();
	int    getSeconds();
	ElapsedTime();
	friend std::ostream &operator<<(std::ostream &os, const ElapsedTime &et);
	ElapsedTime          operator=(const ElapsedTime &et);
};

class GameState : public Serializer {
  private:
	// Magic : 43 42 53 00 (CBS\0 : Cudoku Board Start)
	const uint32_t GameStateMagic = 0x434253;

	// Level of difficulty of the board
	// TODO: Not yet implemented
	uint8_t Difficulty = 0;

	// Elapsed time
	ElapsedTime Elapsed;

	// Value of each cell on the board
	// Since each cell can be represented
	// in 4bits only, each byte will
	// contain information for two adjacent
	// cells.
	uint8_t numbers[41];

	// Position of predefined cells on the
	// board.
	// Each byte will contain information
	// for 8 adjacent cells.
	uint8_t pred[11];

	// Position of the cursor at the moment
	// of save
	uint8_t PosX, PosY;

	uint32_t Magic();
	void     loadFromBytes();
	void     storeIntoBytes();

  public:
	size_t   size();
	GameState();
	GameState(Sudoku *s, const ElapsedTime &e, int diff, uint8_t x, uint8_t y);
	void update(Sudoku *s, const ElapsedTime &e, int diff, uint8_t x,
	            uint8_t y);
	friend std::ostream &operator<<(std::ostream &os, const GameState &gs);
	void                 populateSudoku(Sudoku *s);
	ElapsedTime          getElapsedTime();
	void                 getPosition(uint8_t *x, uint8_t *y);
};

class CudokuSaveFile : public Serializer {
	// Magic : 43 53 46 00 (CSF\0 : Cudoku Save File)
	const uint32_t CudokuSaveFileMagic = 0x43534600;

	// Delimiter to stop 'cat'ing the file as text
	// Taken from the PNG file format (Wikipedia)
	// 0D 0A    A DOS-style line ending (CRLF) to detect DOS-Unix line ending
	// conversion of the data.
	// 1A       A byte that stops display of the file under DOS when the command
	// type has been used—the end-of-file character.
	// 0A       A Unix-style line ending (LF) to detect Unix-DOS line ending
	// conversion.
	const uint32_t Delimiter = 0x0D0A1A0A;

	// Size of the binary to prevent
	// tampering and stop reading
	uint64_t Size;

	// Checksum on the whole binary data
	uint64_t Checksum;

	// Version Info
	// Binaries with same major versions
	// should be both backward and forward
	// compatible
	const uint8_t VMajor = VMAJOR;
	const uint8_t VMinor = VMINOR;

	// Number of saved games
	uint8_t SaveCount;

	// Array of saved games
	std::vector<GameState> States;

	// Array of HiScores
	// Three of each difficulty level,
	// from easy to hard
	// In each difficulty level, from
	// shortest to longest
	ElapsedTime HiScores[9];

	void     loadFromBytes();
	void     storeIntoBytes();
	uint32_t Magic();

  public:
	size_t   size();
	CudokuSaveFile();
	friend std::ostream &operator<<(std::ostream &        os,
	                                const CudokuSaveFile &csf);
	// loadLastGame will return the game number which can be used
	// to overwrite the previous state of the saved game with the
	// new one, instead of creating a new save game each time
	int loadLastGame(Sudoku *s, ElapsedTime &e, int *x, int *y);
	// Create a new save game with the given parameters.
	// If GameNo != -1, then don't create a new save game, instead, update the
	// old one at GameNo index.
	// The returned integer will denote the new index of the game if changed.
	int saveGame(Sudoku *s, const ElapsedTime &et, int Difficulty, uint8_t x,
	             uint8_t y, int GameNo = -1);

	// Actual I/O helpers.
	// These might be better off if gone into Serializer class directly,
	// but I'm not entirely sure what will be serialization process then,
	// so they are here for now.
	void loadFromFile(const char *loc);
	void storeIntoFile(const char *loc);
};
