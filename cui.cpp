#include "cui.h"
#include "cdsf.h"
#include "optionparser.h"
#include "sudoku.h"
#include <iostream>
#include <locale.h>
#include <ncurses.h>
#include <signal.h>
#include <unistd.h>
using namespace std;

static const char *thin_bar            = "\u2502";
static const char *thin_top            = "\u2500";
static const char *thin_junction       = "\u253c";

#if 0
static const char *thin_topleft        = "\u250c";
static const char *thin_topright       = "\u2510";
static const char *thin_bottomleft     = "\u2514";
static const char *thin_bottomright    = "\u2518";
static const char *thin_sideleft       = "\u252c";
static const char *thin_sideright      = "\u2524";
static const char *thin_topjunction    = "\u252c";
static const char *thin_bottomjunction = "\u2534";
#endif

static const char *bold_bar            = "\u2503";
static const char *bold_top            = "\u2501";
static const char *bold_topleft        = "\u250f";
static const char *bold_topright       = "\u2513";
static const char *bold_bottomleft     = "\u2517";
static const char *bold_bottomright    = "\u251b";
static const char *bold_sideleft       = "\u2523";
static const char *bold_sideright      = "\u252b";
static const char *bold_junction       = "\u254b";
static const char *bold_topjunction    = "\u2533";
static const char *bold_bottomjunction = "\u253b";

#define topleft bold_topleft
#define topright bold_topright
#define bottomleft bold_bottomleft
#define bottomright bold_bottomright
#define sideleft bold_sideleft
#define sideright bold_sideright
#define topjunction bold_topjunction
#define bottomjunction bold_bottomjunction

#define junction \
	((((j + 3) % 12 == 0) || (i % 6 == 0)) ? bold_junction : thin_junction)
#define bar (j % 12 == 0 ? bold_bar : thin_bar)
#define top (i % 6 == 0 ? bold_top : thin_top)

#define coord(x, y) (x + 1), ((y * 2) + 2)

#define px(x) (x + 1)
#define py(y) (y * 2) + 2

static int         sx = 0, sy = 0;
static const char *strings[] = {" ", "1", "2", "3", "4",
                                "5", "6", "7", "8", "9"};
// A simple struct to hold the
// location of the cursor in the
// Sudoku. x and y are always with
// respect to the 9x9 grid. 2,1 means
// third row and second column of the
// Sudoku.
struct Cursor {
	int x, y;
};

// SaveGame
static CudokuSaveFile csf    = CudokuSaveFile();
static int            GameNo = -1;
static ElapsedTime    TimeElapsed;
// Options
static OptionParser optionParser = OptionParser();
static Options      options;
struct timespec     RefreshSleep;
// To restore cursor in case of a resize
static Cursor  presentCur    = {0, 0};
static Sudoku *presentSudoku = NULL;
static bool    noDraw        = false;
static int     numResizing   = 0; // total number of resize requests
// To remove 'incorrect solution' message after a check
static bool lastError = false;
// To store the length of the last message
static size_t lastLen = 0;
// Colors
#define COLOR_INFO COLOR_PAIR(1)
#define COLOR_WARN COLOR_PAIR(2)
#define COLOR_ERR COLOR_PAIR(3)
#define COLOR_HIGHLIGHT COLOR_PAIR(4)

// Given a frequency in hertz, the function
// calculates the required sleep time in
// nanoseconds for each iteration of the loop
static long calculateRefreshSleep(int freq) {
	double secondPerFrame = 1e9 * 1.0 / freq;
	return (long)secondPerFrame;
}

// Sets the cell pointed by the cursor
// to the specified value
static void set_cell(Cursor c, int val) {
	if(noDraw)
		return;
	move(sx + c.x * 2 + 1, sy + c.y * 4 + 1);
	addch(' ');
	move(sx + c.x * 2 + 1, sy + c.y * 4 + 3);
	addch(' ');
	mvaddstr(sx + c.x * 2 + 1, sy + c.y * 4 + 2, strings[val]);
}

// Moves the keyboard cursor to the specified cell
static void move_to_cell(Cursor *c) {
	presentCur = *c;
	if(noDraw)
		return;
	move(sx + c->x * 2 + 1, sy + c->y * 4 + 2);
}

// type = 1 -> Info
//        2 -> Warning
//        3 -> Error
static void show_message(string s, int color = COLOR_INFO) {
	if(noDraw)
		return;
	int    ix   = sx - 1;
	size_t nlen = s.length();
	int    iy   = sy + 18 - (nlen / 2);
	if(lastLen != 0) {
		int oy = sy + 18 - (lastLen / 2);
		attroff(A_BOLD | color);
		while(lastLen--) {
			mvaddstr(ix, oy, " ");
			oy++;
		}
	}
	attron(A_BOLD | color);
	mvaddstr(ix, iy, s.c_str());
	attroff(A_BOLD | color);
	lastLen = nlen;
}

// Moves the cursor to the 'next' 'free' cell in
// the Sudoku, i.e. the next cell of the Sudoku
// which was not predefined by the generator.
static void move_to_next_free(Sudoku *s, Cursor *c) {
	int i = c->x, j = c->y;

	// Check if j is already pointing
	// to the last cell of an row
	if(j == 8) {
		// It is, so increment i to
		// point to the next row, wrapping
		// over to the first one
		i = (i + 1) % 9;
		j = 0;
	} else
		j++; // Just point to the next cell of that row

	// We will search for an user accessible cell
	// until we do not get back to the original one
	for(; i != c->x || j != c->y; i = (i + 1) % 9) {
		for(; j < 9; j++) {
			if(!s->is_predefined(i, j)) {
				c->x = i;
				c->y = j;
				move_to_cell(c);
				return;
			}
		}
		j = 0;
	}
}

// Moves the cursor to the 'previous' 'free' cell in
// the Sudoku, i.e. the previous cell of the Sudoku
// which was not predefined by the generator.
static void move_to_prev_free(Sudoku *s, Cursor *c) {
	int i = c->x, j = c->y;

	// Check if j is already pointing
	// to the first cell of an row
	if(j == 0) {
		// It is, so decrement i to
		// point to the previous row, wrapping
		// over to the last one
		i = i == 0 ? 8 : i - 1;
		j = 8;
	} else
		j--; // Just point to the previous cell of that row

	// We will search for an user accessible cell
	// until we do not get back to the original one
	for(; i != c->x || j != c->y;) {
		for(; j >= 0; j--) {
			if(!s->is_predefined(i, j)) {
				c->x = i;
				c->y = j;
				move_to_cell(c);
				return;
			}
		}
		j = 8;
		i--;
		if(i == -1) // Wrap i over
			i = 8;
	}
}

// Moves the cursor to the 'nearest' cell in upward
// direction from the present cursor position.
// The nearest cell is determined by the following :
// 1. First, we jump one row up in the same column,
//      and start checking for free cells in that
//      row to the right. If we find one, we note
//      that index.
// 2. We perform the search from (column - 1) to 0,
//      and again note that index if we find one.
// 3. Finally, we compare the two indices if
//      at least one of them is not -1. If one of
//      them is -1, we set the nearest index to the
//      other one. Otherwise we calculate the
//      relative distance of both the indice
//      from the given column, and set nearest
//      to the lowest one.
// 4. If both of them is -1, then set the row
//      to be next, wrapping up the last row,
//      and continue from step 1 unless we
//      end up in the same row where we
//      started from.
static void move_to_nearest_free_up(Sudoku *s, Cursor *c) {
	int i = c->x;
	if(i == 0) {
		i = 8;
	} else
		i--;
	do {

		int nearest_left = -1, nearest_right = -1, nearest = 0;
		// Right search
		for(int k = c->y; k < 9; k++) {
			if(!s->is_predefined(i, k)) {
				nearest_right = k;
				break;
			}
		}
		// Left search
		for(int k = c->y - 1; k >= 0; k--) {
			if(!s->is_predefined(i, k)) {
				nearest_left = k;
				break;
			}
		}
		// Check if atleast 1 of them is not -1
		if(nearest_right != -1 || nearest_left != -1) {
			// If one is -1, nearest is the other one
			if(nearest_right == -1)
				nearest = nearest_left;
			else if(nearest_left == -1)
				nearest = nearest_right;
			else { // Count the least distance from present cell
				int ry = (nearest_right - c->y);
				int ly = (c->y - nearest_left);
				if(ly > ry)
					nearest = nearest_right;
				else
					nearest = nearest_left;
			}
			// Set the cursor to the nearest cell
			c->x = i;
			c->y = nearest;
			move_to_cell(c);
			return;
		}
		// Keep looking
		if(i == 0) {
			i = 8;
		} else
			i--;
	} while(i != c->x);
}

static void move_to_nearest_free_down(Sudoku *s, Cursor *c) {
	int i = c->x;
	i     = (i + 1) % 9;
	do {

		int nearest_left = -1, nearest_right = -1, nearest = 0;

		for(int k = c->y; k >= 0; k--) {
			if(!s->is_predefined(i, k)) {
				nearest_left = k;
				break;
			}
		}

		for(int k = c->y + 1; k < 9; k++) {
			if(!s->is_predefined(i, k)) {
				nearest_right = k;
				break;
			}
		}

		if(nearest_right != -1 || nearest_left != -1) {
			if(nearest_right == -1)
				nearest = nearest_left;
			else if(nearest_left == -1)
				nearest = nearest_right;
			else {
				int ry = (nearest_right - c->y);
				int ly = (c->y - nearest_left);
				if(ly > ry)
					nearest = nearest_right;
				else
					nearest = nearest_left;
			}
			c->x = i;
			c->y = nearest;
			move_to_cell(c);
			return;
		}
		i = (i + 1) % 9;
	} while(i != c->x);
}

static bool draw_box() {
	int h, w;
	int width  = 36;
	int height = 18;
	getmaxyx(stdscr, h, w);
	if(h < 22 || w < 40) {
		int msgx = h / 2;
		int msgy = 0;
		if(w > 42)
			msgy = w / 2 - 21;
		mvaddstr(msgx, msgy, "Increase the window size to atleast 40x22!");
		return false;
	}
	h /= 2;
	w /= 2;
	h -= 9;
	w -= 18;
	sx = h;
	sy = w;
	move(h, w);
	for(int i = 0; i <= height; i++) {
		move(h + i, w);
		if(i % 2 == 0) {
			if(i == 0)
				addstr(topleft);
			else if(i == height)
				addstr(bottomleft);
			else
				addstr(sideleft);
			for(int j = 1; j < width; j += 4) {
				move(h + i, w + j);
				addstr(top);
				addstr(top);
				addstr(top);
				move(h + i, w + j + 3);
				if(j != width - 3) {
					if(i == 0)
						addstr(topjunction);
					else if(i == height)
						addstr(bottomjunction);
					else
						addstr(junction);
				}
			}
			if(i == 0)
				addstr(topright);
			else if(i == height)
				addstr(bottomright);
			else
				addstr(sideright);
		} else {
			for(int j = 0; j <= width; j += 4) {
				move(h + i, w + j);
				addstr(bar);
			}
		}
	}
	return true;
}

static void draw_with_highlight(Sudoku *s, Cursor *orig, int previousValue,
                                bool animate = false, int resizeId = 0) {
	Cursor cur         = {0, 0};
	int    toHighlight = s->at(orig->x, orig->y);
	// if animate is false and toHighlight is zero,
	// then the user just moved to an empty position,
	// so we don't need to draw the whole sudoku,
	// we just need to turn off highlighting of the
	// previous elements
	if(!animate && !toHighlight) {
		// Also, if previous cell was empty, we don't
		// need to do anything
		if(previousValue == 0)
			return;
		// Just find the previous value, turn it off,
		// and return
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				int val = s->at(i, j);
				if(val == previousValue) {
					cur.x = i;
					cur.y = j;
					attroff(A_STANDOUT | COLOR_HIGHLIGHT);
					if(s->is_predefined(i, j)) {
						attron(A_BOLD);
					}
					set_cell(cur, val);
					attroff(A_BOLD);
				}
			}
		}
		move_to_cell(orig);
		return;
	}
	bool doHighlight =
	    !s->is_predefined(orig->x, orig->y) && s->at(orig->x, orig->y);
	// Otherwise, if animate is false, i.e.
	// it is not the first draw, then
	// just redraw either the previously
	// highlighted boxes, or the newly
	// selected ones.
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j < 9; j++) {
			if(resizeId != numResizing)
				return;
			int val = s->at(i, j);
			cur.x   = i;
			cur.y   = j;
			if(doHighlight && val == toHighlight) {
				// Turn on the present stuff
				attron(COLOR_HIGHLIGHT);
				if(!(i == orig->x && j == orig->y)) {
					attron(A_STANDOUT);
				}
				if(s->is_predefined(i, j)) {
					attron(A_BOLD);
				}
				set_cell(cur, val);
				attroff(COLOR_HIGHLIGHT | A_STANDOUT | A_BOLD);
			} else if(val == previousValue || animate) {
				// Just turn off the previous stuff
				if(s->is_predefined(i, j)) {
					attron(A_BOLD);
				}
				set_cell(cur, val);
				attroff(A_BOLD);
			}
			// don't animate on resize or when disabled by options.conf
			if(animate && resizeId == 0 && options.Animate) {
				refresh();
				struct timespec t;
				t.tv_nsec = 1e7;
				t.tv_sec  = 0;
				nanosleep(&t, NULL);
			}
		}
	}
	move_to_cell(orig);
}

static void init_options() {
	def_prog_mode();
	endwin();
	options              = optionParser.parse();
	reset_prog_mode();
	refresh();
	RefreshSleep.tv_nsec = calculateRefreshSleep(options.RefreshRate);
	if(has_colors() && options.UseColors) {
		init_pair(1, COLOR_GREEN, -1);
		init_pair(2, COLOR_YELLOW, -1);
		init_pair(3, COLOR_RED, -1);
		int cols[] = {COLOR_RED, COLOR_BLUE, COLOR_GREEN, COLOR_YELLOW};
		init_pair(4, cols[options.HighlightColor], -1);
	} else {
		init_pair(1, -1, -1);
		init_pair(2, -1, -1);
		init_pair(3, -1, -1);
		init_pair(4, -1, -1);
	}
}

typedef void (*movementHandler)(Sudoku *s, Cursor *cur);

static void handleMovement(movementHandler m, Sudoku *s, Cursor *cur) {
	int old = s->at(cur->x, cur->y);
	m(s, cur);
	draw_with_highlight(s, cur, old, false);
}

static void handle_keypress(Sudoku *s, Cursor *cur, int c) {
	if(lastError) {
		show_message("");
		lastError = false;
	}
	switch(c) {
		case KEY_LEFT:
		case 'a':
		case 'A': handleMovement(move_to_prev_free, s, cur); break;
		case KEY_RIGHT:
		case 'd':
		case 'D': handleMovement(move_to_next_free, s, cur); break;
		case KEY_UP:
		case 'w':
		case 'W': handleMovement(move_to_nearest_free_up, s, cur); break;
		case KEY_DOWN:
		case 's':
		case 'S': handleMovement(move_to_nearest_free_down, s, cur); break;

		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9': {
			int old = s->at(cur->x, cur->y);
			set_cell(*cur, c - '0');
			s->set(cur->x, cur->y, c - '0');
			draw_with_highlight(s, cur, old);
			break;
		}
		case '0':
		case 'x':
		case 'X':
		case 127:
		case '\b':
		case KEY_BACKSPACE:
		case KEY_DC: {
			int old = s->at(cur->x, cur->y);
			set_cell(*cur, 0);
			s->set(cur->x, cur->y, 0);
			draw_with_highlight(s, cur, old);
			move_to_cell(cur);
			break;
		}
		case 'c':
		case 'C': {
			int old = s->at(cur->x, cur->y);
			if(!s->check(&cur->x, &cur->y)) {
				show_message("Incorrect solution!", COLOR_ERR);
				lastError = true;
				int val   = s->at(cur->x, cur->y);
				draw_with_highlight(s, cur, old);
				attron(COLOR_ERR | A_STANDOUT);
				set_cell(*cur, val);
				move_to_cell(cur);
				attroff(COLOR_ERR | A_STANDOUT);
			} else
				show_message("Solved!");
			break;
		}
		case 'n':
		case 'N': {
			s->reset();
			show_message("Generating new sudoku..");
			refresh();
			if(!s->generate()) {
				cur->x = cur->y = 0;
				draw_with_highlight(s, cur, 0, true);
				return;
			}
			s->remove_at_random(50);
			cur->x = cur->y = 0;
			draw_with_highlight(s, cur, 0, true);
			refresh();
			show_message("");
			cur->y = -1;
			move_to_next_free(s, cur);
			refresh();
			TimeElapsed.reset();
			break;
		}
		case 'r': // Reload the config
		case 'R': init_options(); break;
	}
}

static void loadSave() {
	def_prog_mode();
	endwin();
	try {
		csf.loadFromFile("savegame.csf");
	} catch(ReadError r) {
		cout << "[Error] ";
		switch(r) {
			case FILE_NOT_FOUND: cout << "File not found!" << endl; break;
			case MAGIC_NOT_MATCHED:
				cout << "Magic not matched! Is it actually a Cudoku "
				        "save file?"
				     << endl;
				break;
			case INVALID_DIFFICULTY:
				cout << "Invalid difficulty level! File is corrupted!" << endl;
				break;
			case MAJOR_VERSION_MISMATCH:
				cout << "This version of the saved file cannot be "
				        "loaded!"
				     << endl;
				break;
			case MINOR_VERSION_MISMATCH:
				cout << "This version of the saved file should be "
				        "compatible with this program, but the "
				        "compatibility layer is not yet implemented!"
				     << endl;
				break;
			case DELIMITER_MISMATCH:
				cout << "File is not delimited with the expected bytes! File "
				        "is corrupted!"
				     << endl;
				break;
			case FILESIZE_LESS_THAN_MINIMUM:
			case SIZE_MISMATCH: break;
		}
		cout << "[Press any key to continue]" << endl;
		getchar();
	}
	reset_prog_mode();
	refresh();
	GameNo = csf.loadLastGame(presentSudoku, TimeElapsed, &presentCur.x,
	                          &presentCur.y);
}

static void showElapsedTime() {
	static char timeString[24];
	static bool lastTimeShown = true;
	if(options.ShowTime && !noDraw) {
		sprintf(timeString, "Time elapsed : %02d:%02d:%02d",
		        TimeElapsed.getHours(), TimeElapsed.getMinutes(),
		        TimeElapsed.getSeconds());
		mvaddstr(sx - 2, sy + 6, timeString);
		lastTimeShown = true;
	} else if(lastTimeShown) {
		mvaddstr(sx - 2, sy + 6, "\t\t\t\t\t\t");
		lastTimeShown = false;
	}
}

void draw_sudoku(Sudoku *s) {

	presentSudoku = s;

	loadSave();
	clear();
	refresh();

	if(!draw_box()) {
		refresh();
		noDraw = true;
	}

	Cursor cur = presentCur;
	if(cur.x == -1) {
		cur.x = 0;
		cur.y = 0;

		draw_with_highlight(s, &cur, 0, true);

		cur.x = 0;
		cur.y = -1;
		move_to_next_free(s, &cur);
	} else {
		draw_with_highlight(s, &cur, 0, true);
	}

	refresh();
	RefreshSleep.tv_nsec        = calculateRefreshSleep(options.RefreshRate);
	RefreshSleep.tv_sec         = 0;
	int c = 0, frameCount = options.RefreshRate;
	while(1) {
		// Update time only every refreshRate tick
		if(frameCount >= options.RefreshRate) {
			if(!noDraw)
				TimeElapsed.tick();
			showElapsedTime();
			move_to_cell(&cur);
			frameCount = 0;
		}
		frameCount++;

		if((c = getch()) == ERR) {
		} else if(c == 'q' || c == 'Q') {
			if(options.AutoSave) {
				def_prog_mode();
				endwin();
				csf.saveGame(s, TimeElapsed, 0, cur.x, cur.y, GameNo);
				csf.storeIntoFile("savegame.csf");
				reset_prog_mode();
				refresh();
			}
			break;
		} else
			handle_keypress(s, &cur, c);

		nanosleep(&RefreshSleep, NULL);
	}
	clear();
	refresh();
}

static void resizeHandler(int sig) {
	(void)sig;
	endwin();
	refresh();
	clear();
	if(!draw_box()) {
		refresh();
		noDraw = true;
		return;
	}
	noDraw = false;
	refresh();
	if(options.ShowTime)
		showElapsedTime();
	numResizing++;
	draw_with_highlight(presentSudoku, &presentCur,
	                    presentSudoku->at(presentCur.x, presentCur.y), true,
	                    numResizing);
	numResizing--;
}

void driver_init() {
	setlocale(LC_ALL, "");
	initscr();
	cbreak();
	start_color();
	use_default_colors();
	init_options(); // colors are initialized there
	noecho();
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE);
	signal(SIGWINCH, resizeHandler);
}

void driver_exit() {
	nocbreak();
	echo();
	keypad(stdscr, FALSE);
	nodelay(stdscr, FALSE);
	endwin();
}
