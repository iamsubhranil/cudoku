#ifndef CUDOKU_CUI
#define CUDOKU_CUI

#include "sudoku.h"

void driver_init();
void driver_exit();
void draw_sudoku(Sudoku *s);
#endif
