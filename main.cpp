#include "cui.h"
#include "sudoku.h"
#include <iostream>

using namespace std;

#ifndef COUNT
#define COUNT 1000
#endif

int main() {
	Sudoku s;
#ifdef SOLVE
	int arr[9][9] = {{8, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 3, 6, 0, 0, 0, 0, 0},
	                 {0, 7, 0, 0, 9, 0, 2, 0, 0}, {0, 5, 0, 0, 0, 7, 0, 0, 0},
	                 {0, 0, 0, 0, 4, 5, 7, 0, 0}, {0, 0, 0, 1, 0, 0, 0, 3, 0},
	                 {0, 0, 1, 0, 0, 0, 0, 6, 8}, {0, 0, 8, 5, 0, 0, 0, 1, 0},
	                 {0, 9, 0, 0, 0, 0, 4, 0, 0}};
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j < 9; j++) {
			s.set(i, j, arr[i][j], arr[i][j] != 0);
		}
	}
	if(s.solve()) {
		cout << "Done!" << endl;
	} else
		cout << "Failed!" << endl;
	s.print();
	return 0;
#endif
#ifdef BENCH
	for(int i = 0; i < COUNT; i++) {
		s.reset();
		if(!s.generate())
			return 1;
	}
#else
	cout << endl;
	driver_init();
	draw_sudoku(&s);
	driver_exit();
#endif
}
