#include "optionparser.h"
#include <iostream>
#include <sstream>
#include <string.h>

using namespace std;

#define CONFIG_LOC "./cudoku.conf"

unordered_set<string> OptionParser::booleanValues = {"True", "False"};
unordered_set<string> OptionParser::colorValues   = {"Red", "Blue", "Green",
                                                   "Yellow"};
unordered_map<string, OptionParser::Type> OptionParser::optionTypes = {
#define OPTION(n, t, z, r) {#n, t},
#include "options.h"
#undef OPTION
};

OptionParser::OptionParser() {
	location     = CONFIG_LOC;
	optionValues = unordered_map<string, Any>();
}

OptionParser::OptionParser(const char *l) {
	location     = l;
	optionValues = unordered_map<string, Any>();
}

ostream &operator<<(ostream &os, const OptionParser::Token &t) {
	for(int i = 0; i < t.length; i++) os << t.str[i];
	return os;
}

Options OptionParser::parse() {
	Options o = {
#define OPTION(n, t, d, r) d,
#include "options.h"
#undef OPTION
	};
	if(reader.is_open()) {
		reader.close();
	}
	reader.open(location, ios::in);
	if(!reader.is_open())
		return o;

	optionValues.clear();

	stringstream b;
	b << reader.rdbuf();
	buffer = strdup(b.str().c_str());
	head   = buffer;
	b.clear();
	reader.close();

	hasErrors = false;
	line      = 1;

	Token t;
	while((t = nextToken()).type != Eof) {
		switch(t.type) {
			case Key: keyParse(t); break;
			default:
				cout << "[Error] Invalid option : '" << t << "'" << endl;
				highlight(t);
				hasErrors = true;
				skipLine();
				break;
		}
	}

#ifdef DEBUG
	cout << "Loaded options " << endl;
	for(auto i = optionValues.begin(), j = optionValues.end(); i != j; i++) {
		cout << i->first << " -> ";
		switch(optionTypes[i->first]) {
			case Integer: cout << i->second.valInteger; break;
			case Color: cout << i->second.valColor; break;
			case String: cout << i->second.valString; break;
			case Boolean: cout << i->second.valBoolean; break;
		}
		cout << endl;
	}

#endif

#define OPTION(n, t, d, r)                            \
	o.n = optionValues.find(#n) != optionValues.end() \
	          ? optionValues[#n].val##t               \
	          : o.n;
#include "options.h"
#undef OPTION

	if(hasErrors) {
		cout << "[Press Enter to continue]" << endl;
		getc(stdin);
	}
	return o;
}

OptionParser::Token OptionParser::word() {
	const char *bak = head;
	Token       token = {Err, bak, 1, line};
	while((isalnum(*head) || *head == '_') && *head != '\0') head++;
	string s;
	s.assign(bak, head - bak);
	token.length = head - bak;
	if(optionTypes.find(s) != optionTypes.end()) {
		token.type = Key;
	} else if(booleanValues.find(s) != booleanValues.end()) {
		token.type = Bool;
	} else if(colorValues.find(s) != colorValues.end()) {
		token.type = Col;
	} else {
		cout << "[Error] Unknown token '" << s << "'!" << endl;
		highlight(token);
		hasErrors = true;
		skipLine();
	}

	return token;
}

OptionParser::Token OptionParser::integer() {
	const char *bak = head;
	while(*head >= '0' && *head <= '9') head++;
	return {Int, bak, head - bak, line};
}

void OptionParser::skipLine() {
	while(*head != '\n' && *head != '\0') head++;
	if(*head == '\n') {
		line++;
		head++;
	}
}

OptionParser::Token OptionParser::nextToken() {
	if(*head == EOF || *head == '\0') {
		return {Eof, head, 1, line};
	}
	if(isalpha(*head)) {
		return word();
	}
	if(isdigit(*head)) {
		return integer();
	}
	switch(*head) {
		case '=': head++; return {Eq, head - 1, 1, line};
		case '\n': line++;
		case '\r':
		case '\t':
		case ' ': head++; return nextToken();
		case '#': skipLine(); return nextToken();
	}
	cout << "[Error] Unknown character '" << *head << "'!" << endl;
	highlight({Err, head, 1, line});
	head++;
	hasErrors = true;
	skipLine();
	return {Err, head - 1, 3, line};
}

int OptionParser::toInteger(const Token &t) {
	string s;
	s.assign(t.str, t.length);
	return stoi(s);
}

int OptionParser::toColor(const Token &t) {
	switch(*t.str) {
		case 'R': return 0;
		case 'B': return 1;
		case 'G': return 2;
		case 'Y': return 3;
		default: return 1;
	}
}

bool OptionParser::toBoolean(const Token &t) {
	switch(*t.str) {
		case 'T': return true;
		default: return false;
	}
}

void OptionParser::keyParse(const Token &t) {
	Token val = nextToken();
	if(val.type != Eq) {
		cout << "[Error] Expected '=' after option '" << t << "'!" << endl;
		highlight(t);
		hasErrors = true;
		skipLine();
		return;
	}
	val = nextToken();
	if(val.type == Eof) {
		cout << "[Error] Unexpected end of file after option '" << t << "'!"
		     << endl;
		highlight(t);
		hasErrors = true;
		return;
	}
	string s;
	s.assign(t.str, t.length);
	switch(optionTypes[s]) {
#define PARSE_TYPE(otype, ttype)                                         \
	case otype:                                                          \
		if(val.type != ttype) {                                          \
			cout << "[Error] Expected " #otype " value for option " << t \
			     << "!" << endl;                                         \
			hasErrors = true;                                            \
		} else {                                                         \
			optionValues[s] = Any{.val##otype = to##otype(val)};         \
		}                                                                \
		break;
		PARSE_TYPE(Integer, Int)
		PARSE_TYPE(Boolean, Bool)
		PARSE_TYPE(Color, Col)
		default:
			cout << "[Error] Undefined value type : '" << val << "'!" << endl;
			highlight(val);
			hasErrors = true;
			break;
	}
}

void OptionParser::highlight(const Token &t) {
	const char *end = t.str, *start = t.str;
	while(*start != '\n' && start != buffer) start--;
	if(*start == '\n')
		start++;
	const char *bak = start;
	while(*end != '\0' && *end != '\n') end++;
	int ch    = t.str - start + 1;
	int extra = 4; // [:]<space>

	int lbak = t.line;
	while(lbak > 0) {
		extra++;
		lbak /= 10;
	}
	lbak = ch;
	while(lbak > 0) {
		extra++;
		lbak /= 10;
	}

	cout << "[" << t.line << ":" << ch << "] ";
	while(bak < end) {
		cout << *bak;
		bak++;
	}
	cout << endl;
	while(extra--) cout << " ";
	while(start < t.str) {
		cout << " ";
		start++;
	}
	for(int i = 0; i < t.length; i++) cout << "~";
	cout << endl;
}
