#pragma once

#include <fstream>
#include <string>
#include <unordered_map>
#include <unordered_set>

class Options {
  public:
#define OPTION(n, t, d, r) r n;
#include "options.h"
#undef OPTION
};

class OptionParser {

  public:
	OptionParser();
	OptionParser(const char *loc);
	Options parse();
	struct Any {
		std::string valString;
		int         valInteger;
		int         valColor;
		bool        valBoolean;
	};

  private:
	enum Type { Integer, Color, Boolean, String };
	enum TokenType { Key, Eq, Int, Col, Bool, Str, Eof, Err };
	struct Token {
		TokenType   type;
		const char *str;
		long        length;
		int         line;
	};
	// Default dictionaries
	static std::unordered_map<std::string, Type> optionTypes;
	static std::unordered_set<std::string>       booleanValues;
	static std::unordered_set<std::string>       colorValues;
	// static std::unordered_map<std::string, Any>  defaultValues;
	// Parsed options
	std::unordered_map<std::string, Any>         optionValues;
	// Config location
	const char *                                 location;
	// Parser stuff
	char *                                       buffer, *head;
	int                                          line;
	std::ifstream                                reader;
	Token                                        nextToken();
	Token                                        word();
	Token                                        integer();
	void                                         keyParse(const Token &t);
	void                                         highlight(const Token &t);
	bool                                         hasErrors;
	void skipLine(); // skip present line in case of errors
	// Converter methods
	int         toInteger(const Token &t);
	std::string toString(const Token &t);
	bool        toBoolean(const Token &t);
	int         toColor(const Token &t);

	friend std::ostream &operator<<(std::ostream &os, const Token &t);
};
