#ifndef OPTION
#define OPTION(Name, Type, DefaultValue, RawType)
#endif

OPTION(RefreshRate, Integer, 60, int)
OPTION(HighlightColor, Color, 3, int)
OPTION(ShowTime, Boolean, true, bool)
OPTION(Animate, Boolean, true, bool)
OPTION(AutoSave, Boolean, true, bool)
OPTION(UseColors, Boolean, true, bool)
