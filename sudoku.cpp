#include "sudoku.h"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <stack>

using namespace std;

#define cube_idx(x, y) ((x / 3) * 3 + y / 3)
#define cell_idx(x, y) (((x)*9) + (y))
#define opt_idx(x, y) (((x)*9) + (y))
#define used_idx(x, y) cell_idx((x), (y - 1))

bool Sudoku::generate_cube2(int x, int y, int u, int p) {
	if(!generate_cube_it(x, y) || !check()) {
		cout << "[Error] Generation failed  " << x << ", " << y << " to " << u
		     << ", " << p << "!" << endl;
		return false;
	}
	return true;
}

int Sudoku::populate_options(int x, int y) {
	int op = 0;

	int cidx = cube_idx(x, y);

	for(int i = 1; i < 10; i++) {
		const bool used =
		    cube[used_idx(cidx, i)] | row[used_idx(x, i)] | col[used_idx(y, i)];
		if(!used) {
			available_options[opt_idx(cell_idx(x, y), op)] = i;
			op++;
		}
	}
	if(op > 0)
		random_shuffle(&available_options[opt_idx(cell_idx(x, y), 0)],
		               &available_options[opt_idx(cell_idx(x, y), op - 1)]);
	return op;
}

#define NEXT_CELL(x, y)      \
	{                        \
		y++;                 \
		if(y == 9) {         \
			x++;             \
			if(x == 9)       \
				return true; \
			else             \
				y = 0;       \
		}                    \
	}

bool Sudoku::generate_cube_it(int x, int y) {
	int stackPointer = 0;
#define TOP stackPointer - 1
	bool                  nextChoice = false;
	do {
#ifdef DEBUG
		if(stackPointer) {
			cout << "NextChoice : " << nextChoice
			     << "\tAddressStack : " << addressXStack[TOP] << ", "
			     << addressYStack[TOP] << "\tChoiceStack : " << sizeStack[TOP]
			     << ", " << chStack[TOP] << endl;
		}
		print();
#endif
		if(nextChoice) {
			if(stackPointer == 0)
				return false;
			x          = addressXStack[TOP];
			y          = addressYStack[TOP];
			uint8_t &choice = chStack[TOP];
			unmark_as_used(available_options[opt_idx(cell_idx(x, y), choice)],
			               x, y);
			choice++;
			if(sizeStack[TOP] == choice) {
				stackPointer--;
				continue; // next choice is true
			} else {
				mark_as_used(available_options[opt_idx(cell_idx(x, y), choice)],
				             x, y);
				nextChoice = false;
				NEXT_CELL(x, y);
			}
		} else if(rvec[cell_idx(x, y)] == 0) {
			int op = populate_options(x, y);
			if(op > 0) { // there are some options
#ifdef DEBUG
				cout << "Options for " << x << ", " << y << " : ";
				for(int i = 0; i < op; i++) {
					cout << available_options[opt_idx(cell_idx(x, y), i)]
					     << " ";
				}
				cout << endl;
#endif
				int choice = available_options[opt_idx(cell_idx(x, y), 0)];
				mark_as_used(choice, x, y);
				chStack[stackPointer]       = 0;
				sizeStack[stackPointer]     = op;
				addressXStack[stackPointer] = x;
				addressYStack[stackPointer] = y;
				stackPointer++;
				NEXT_CELL(x, y);
			} else { // we need to pop
#ifdef DEBUG
				cout << "No options for " << x << ", " << y << endl;
#endif
				nextChoice = true;
			}
		} else {
			NEXT_CELL(x, y);
		}
	} while(1);
	return false;
}

int Sudoku::rand_digit() {
	return dis(gen);
}

void Sudoku::print_tabs(int i) {
	cout << "\n";
	for(int j = 0; j < i; j++) cout << "  ";
}

void Sudoku::print(const char *name, int nestlevel, unordered_set<int> s1) {
	print_tabs(nestlevel);
	cout << name << " : ";
	for(auto i = s1.begin(); i != s1.end(); i++) cout << *i << " ";
	cout << endl;
}

void Sudoku::print(const char *name, int nestlevel, vector<int> s1) {
	print_tabs(nestlevel);
	cout << name << " : ";
	for(auto i = s1.begin(); i != s1.end(); i++) cout << *i << " ";
	cout << endl;
}

void Sudoku::mark_as_used(int op, int x, int y) {
	cube[used_idx(cube_idx(x, y), op)] = true;
	// Mark as used for the column
	col[used_idx(y, op)] = true;
	// Mark as used for the row
	row[used_idx(x, op)] = true;
	rvec[cell_idx(x, y)] = op;
}

void Sudoku::unmark_as_used(int op, int x, int y) {
	cube[used_idx(cube_idx(x, y), op)] = false;
	// Unmark as used for the column
	col[used_idx(y, op)] = false;
	// Unmark as used for the row
	row[used_idx(x, op)] = false;
	rvec[cell_idx(x, y)] = 0;
}

bool Sudoku::generate_cube_rec(int x, int y, int nestlevel) {
	if(rvec[cell_idx(x, y)] != 0) {

		int nexty = y + 1;
		int nextx = x;
		if(nexty == 9) {
			nexty = 0;
			nextx++;
			if(nextx == 9)
				return true;
		}
		return generate_cube_rec(nextx, nexty, nestlevel);
	}
	vector<int> options;

#ifdef DEBUG
	// print("elements of set ", nestlevel, puse);
#endif

	for(int i = 1; i < 10; i++) {
#ifdef DEBUG
		print_tabs(nestlevel);
		cout << "Checking " << i << " : ";
#endif
		if(!cube[used_idx(cube_idx(x, y), i)] && !row[used_idx(x, i)] &&
		   !col[used_idx(y, i)]) {
#ifdef DEBUG
			cout << " : Not found" << endl;
#endif
			options.push_back(i);
		}
#ifdef DEBUG
		else
			cout << " : Found" << endl;
#endif
	}
	random_shuffle(options.begin(), options.end());
#ifdef DEBUG

	print_tabs(nestlevel);
	cout << "--> Cell : " << x << ", " << y << " Cube : " << (x / 3 + y / 3)
	     << endl;
	print("Options              ", nestlevel, options);
#endif
	int nexty = y + 1;
	int nextx = x;
	if(nexty == 9) {
		nexty = 0;
		nextx++;
	}

	for(auto op : options) {
#ifdef DEBUG
		print_tabs(nestlevel);
		cout << "Option for " << x << ", " << y << " is " << op << " .. ";

		if(!check(x, y, op)) {
			cout << "\n";
			print();
			print_tabs(nestlevel);
			cout << "[Error] Property violated at " << x << ", " << y
			     << " with value " << op << endl;
			getchar();
		}
#endif
		mark_as_used(op, x, y);
		if(nextx == 9 || generate_cube_rec(nextx, nexty, nestlevel + 1)) {
#ifdef DEBUG
			print_tabs(nestlevel);
			cout << op << " accepted at " << x << ", " << y << endl;
#endif
			return true;
		}
#ifdef DEBUG
		print_tabs(nestlevel);
		cout << op << " denied at " << x << ", " << y << endl;
		print();
		getchar();
#endif
		unmark_as_used(op, x, y);
	}
#ifdef DEBUG
	print_tabs(nestlevel);
	if(options.empty())
		cout << "[No Option] ";
	cout << "Returning from " << x << ", " << y << endl;
#endif
	return false;
}

void Sudoku::generate_cube_nocheck(int x, int y) {
	bool inserted[10] = {false};
	for(int i = x; i < x + 3; i++) {
		for(int j = y; j < y + 3; j++) {
			int d;
			do {
				d = rand_digit();
			} while(inserted[d]);
			inserted[d]          = true;
			mark_as_used(d, i, j);
		}
	}
}

Sudoku::Sudoku() {
	/*
	for(int i = 0; i < 9; i++) {
	    for(int j = 0; j < 9; j++) {
	        rvec[cell_idx(i, j)] = 0;
	        predefined[cell_idx(i, j)] = true;
	        cube[used_idx(i, j + 1)] = false;
	        row[used_idx(i, j + 1)]  = false;
	        col[used_idx(i, j + 1)]  = false;
	    }
	}
	gen = mt19937(rd());
	dis = uniform_int_distribution<>(1, 9);
	*/
	reset();
}

bool Sudoku::check(int i, int j, int el, bool silent) {
	// Check at cube
	int cx = (i / 3) * 3;
	int cy = (j / 3) * 3;
#ifdef DEBUG
	cout << "Cube start at " << cx << ", " << cy << " for idx " << i << ", "
	     << j << endl;
#endif
	for(int k = cx; k < cx + 3; k++) {
		for(int l = cy; l < cy + 3; l++) {
			if(k != i && l != j && el == rvec[cell_idx(k, l)]) {
				if(!silent)
					cout << "[Error] Found " << el << " inside the cube at "
					     << k << ", " << l << endl;
				return false;
			}
		}
	}
	// Check at rows
	for(int k = 0; k < 9; k++) {
		if(k != i && rvec[cell_idx(k, j)] == el) {
			if(!silent)
				cout << "[Error] Found " << el << " in row " << k << endl;
			return false;
		}
	}
	// Check at cols
	for(int k = 0; k < 9; k++) {
		if(k != j && rvec[cell_idx(i, k)] == el) {
			if(!silent)
				cout << "[Error] Found " << el << " inside col " << k << endl;
			return false;
		}
	}
	return true;
}

bool Sudoku::check(int *x, int *y, bool checkOnlyUser) {
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j < 9; j++) {
			if(checkOnlyUser && is_predefined(i, j))
				continue;
			if(!check(i, j, rvec[cell_idx(i, j)])) {
				if(x != NULL) {
					*x = i;
					*y = j;
				}
				return false;
			}
		}
	}
	return true;
}

bool Sudoku::generate() {
	///*
	generate_cube_nocheck(0, 0);
	generate_cube_nocheck(3, 3);
	generate_cube_nocheck(6, 6);
	//*/
	return generate_cube2(0, 0, 9, 9);
}

void Sudoku::print() {
	for(int i = 0; i < 9; i++) {
		for(int k = 0; k < 9; k++) {
			int j = rvec[cell_idx(i, k)];
			if(j == 0)
				cout << "--"
				     << " ";
			else
				cout << setw(2) << j << " ";
		}
		cout << "\n\n";
	}
}

void Sudoku::remove_at_random(int num) {
	for(int k = 0; k < num; k++) {
		int i = rand_digit() % 9, j = rand_digit() % 9;
		if(rvec[cell_idx(i, j)] == 0)
			k--;
		else {
			rvec[cell_idx(i, j)] = 0;
			predefined[cell_idx(i, j)] = false;
		}
	}
}

bool Sudoku::is_predefined(int i, int j) {
	return predefined[cell_idx(i, j)];
}

int Sudoku::at(int i, int j) {
	return rvec[cell_idx(i, j)];
}

void Sudoku::set(int i, int j, int val, bool isPredefined) {
	rvec[cell_idx(i, j)] = val;
	predefined[cell_idx(i, j)] = isPredefined;
	if(isPredefined) {
		mark_as_used(rvec[cell_idx(i, j)], i, j);
	}
}

void Sudoku::reset() {
#define fill_zero(name) fill(name, name + 81, 0)
	fill_zero(rvec);
	fill(predefined, predefined + 81, 1);
	fill_zero(row);
	fill_zero(col);
	fill_zero(cube);
	fill(available_options, available_options + 729, 0);
	gen = mt19937(rd());
	dis = uniform_int_distribution<>(1, 9);

	// Sudoku();
}

bool Sudoku::solve() {
	return generate_cube_it(0, 0) && check();
}
