#ifndef CUDOKU_H
#define CUDOKU_H
#include <algorithm>
#include <random>
#include <unordered_set>
#include <vector>

class Sudoku {
  private:
	uint8_t rvec[81];
	bool predefined[81]; // marks which cells are predefined and uneditable
	bool cube[81]; // values used in each cube, cube[used_idx(i, j)] is true if
	               // j is in ith cube
	bool row[81];  // values used in each row, row[used_idx(i, j)] is true if
	               // j is in ith row
	bool col[81];  // values used in each column, col[used_idx(i, j)] is true
	               // if j is in ith column
	uint8_t available_options[729]; // available options for each cell

	// raw stacks used for generation
	uint8_t chStack[81], sizeStack[81], addressXStack[81], addressYStack[81];
	// random stuff
	std::random_device              rd;
    std::mt19937                    gen;
    std::uniform_int_distribution<> dis;

	int  rand_digit();
	bool generate_cube_it(int x, int y);
	// populates available options for a
	// cell, returns number of options
	int  populate_options(int x, int y);
	bool generate_cube2(int x, int y, int u, int p);
	void print_tabs(int i);
	void print(const char *name, int nestlevel, std::unordered_set<int> s1);
	void print(const char *name, int nestlevel, std::vector<int> s1);
	void mark_as_used(int op, int x, int y);
	void unmark_as_used(int op, int x, int y);
	bool generate_cube_rec(int x, int y, int nestlevel = 0);
	void generate_cube_nocheck(int x, int y);

  public:
	Sudoku();
	bool check(int i, int j, int el, bool silent = true); // silent : don't print messages to stdout
	bool check(int *x = NULL, int *y = NULL, bool checkOnlyUser = true); // checkOnlyUser : don't check predefined cells
	bool generate();
	void print();
	void remove_at_random(int num);
	int at(int i, int j);
	void set(int i, int j, int val, bool isPredefined = false);
	bool is_predefined(int i, int j);
	void reset();
	// A wrapper around generate_cube_it,
	// which tries to find a solution
	// with the given set of elements
	bool solve();
	Sudoku operator=(const Sudoku &right);
};

#endif
